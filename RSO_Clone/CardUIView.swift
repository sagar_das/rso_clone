//
//  CardUIView.swift
//  RSO_Clone
//
//  Created by Sagar Das on 6/29/20.
//  Copyright © 2020 RSO_CloneOrganizationName. All rights reserved.
//

import SwiftUI

struct CardUIView: View {
    
    let card: Card?
    
    var body: some View {
        
        ZStack{
            RoundedRectangle(cornerRadius: 25, style: .continuous)
                .fill(Color.white)
                .shadow(radius: 10)
            
            VStack{
                Text(card!.name!)
                    .font(.largeTitle)
                    .foregroundColor(.black)
                
                Text(card!.description!)
                    .font(.title)
                    .foregroundColor(.gray)
                
            }
        .padding(20)
            .multilineTextAlignment(.center)
        }
        .frame(width: 450, height: 250)
    }
}

struct CardUIView_Previews: PreviewProvider {
    static var previews: some View {
        CardUIView(card: Card.example)
    }
}
