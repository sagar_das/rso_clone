import SwiftUI

struct ContentView: View {

    init() {
        UITabBar.appearance().backgroundColor = UIColor.purple
    }

    @State var selected = 3

    var body: some View {
        TabView(selection: $selected) {
            OrdersView().tabItem({
                Image(systemName: Constants.TabBarImageName.tabBar0)
                        .font(.title)
                Text("\(Constants.TabBarText.tabBar0)")
            }).tag(0)

            AccountsView().tabItem({
                Image(systemName: Constants.TabBarImageName.tabBar1)
                        .font(.title)
                Text("\(Constants.TabBarText.tabBar1)")
            }).tag(1)

            CartView().tabItem({
                Image(systemName: Constants.TabBarImageName.tabBar2)
                        .font(.title)
                Text("\(Constants.TabBarText.tabBar2)")
            }).tag(2)

            CatalogView().tabItem({
                Image(systemName: Constants.TabBarImageName.tabBar3)
                        .font(.title)
                Text("\(Constants.TabBarText.tabBar3)")
            }).tag(3)
        }.accentColor(Color.red)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
