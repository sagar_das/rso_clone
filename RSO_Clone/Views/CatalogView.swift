//
// Created by Sagar Das on 8/10/20.
// Copyright (c) 2020 RSO_CloneOrganizationName. All rights reserved.
//
import SwiftUI

struct CatalogView: View {
    var body: some View {
        VStack(spacing: 50) {
            StoreItem(image: "image2", text1: "Rebeloper Store", text2: "The Ultimate In-app Purchases Guide for iOS and Swift")
            Divider()
            StoreItem(image: "image3", text1: "YouTube Channel Resources", text2: "Code Better and Get More Downloads")
        }

                .padding(20)
    }
}

struct CatalogView_Previews: PreviewProvider {
    static var previews: some View {
        CatalogView()
    }
}

struct StoreItem : View {

    var image : String
    var text1 : String
    var text2 : String

    var body : some View {
        return
                VStack {
                    Image(image)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .cornerRadius(10)
                            .shadow(radius: 10)
                    VStack(alignment: .leading) {
                        Text(text1)
                                .font(.title)
                        //                    .lineLimit(2)
                        Text(text2)
                                .font(.caption)
                                .foregroundColor(.gray)
                    }
                }
    }
}

