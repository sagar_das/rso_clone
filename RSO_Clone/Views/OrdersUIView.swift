//
// Created by Sagar Das on 8/10/20.
// Copyright (c) 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class OrdersUIView: UITableViewCell{

    var name: String?
    var desc: String?
//    var img: UIImage?
    //var dataCard: Card?

    var cardView = CardView()
    var nameView : UITextView = {
        var textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()

    var descView : UITextView = {
        var descTextView = UITextView()
        descTextView.translatesAutoresizingMaskIntoConstraints = false
        return descTextView
    }()

    var imgView : UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()









    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {


        super.init(style: style, reuseIdentifier: reuseIdentifier)




        self.addSubview(cardView)
        self.addSubview(imgView)
        self.addSubview(nameView)
        self.addSubview(descView)







        imgView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        imgView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        imgView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        imgView.widthAnchor.constraint(equalTo: self.heightAnchor).isActive = true

        nameView.leftAnchor.constraint(equalTo: self.imgView.rightAnchor).isActive = true
        nameView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        nameView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        nameView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true

//        descView.leftAnchor.constraint(equalTo: self.imgView.rightAnchor).isActive = true
//        descView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
//        descView.bottomAnchor.constraint(equalTo: self.nameView.bottomAnchor).isActive = true
        descView.topAnchor.constraint(equalTo: self.nameView.bottomAnchor).isActive = true







    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if let name = name {
            nameView.text = name
        }
        if let desc = desc {
            descView.text = desc
        }
//        if let img = img {
//            imgView.image = img
//        }


    }

    required init?(coder aDecoder: NSCoder) {

        fatalError("init(coder) has not be implemented")
    }

    @objc func onCheckBoxValueChange(_ sender: CheckBox) {

        print(sender.isChecked)
    }



}
