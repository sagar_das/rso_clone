/*
 Copyright (c) 2015-present, salesforce.com, inc. All rights reserved.
 
 Redistribution and use of this software in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this list of conditions
 and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of
 conditions and the following disclaimer in the documentation and/or other materials provided
 with the distribution.
 * Neither the name of salesforce.com, inc. nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior written
 permission of salesforce.com, inc.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import Foundation
import UIKit
import SalesforceSDKCore
import SmartStore



class RootViewController: UITableViewController {
    
    //var dataRows = [[String: Any]]()
    var dataRows = [NSDictionary]()
    var store = SmartStore.shared(withName: SmartStore.defaultStoreName)
    let mylog = OSLog(subsystem: "com.commercecx.rso", category: "tutorial")
    var cell: CustomCell!
    var rowHeight : CGFloat!
    var imageUtils = ImageUtils()
    var networkUtils = NetworkUtils()
    var requestUtils = RequestUtils()
    var utils = UtilOperations()
    var indicator: UIActivityIndicatorView!
    var btnFloat: UIButton!
    var soupName: String!
    var filteredRecords: [[String:Any]]!
    


    
    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()

        indicator = utils.fetchLoadingBar()




        let request = requestUtils.createRestRequest(path: networkUtils.productPath, type:"GET")
        

        
        RestClient.shared.send(request: request) { [weak self] (result) in
            switch result {
                case .success(let response):
                    self?.indicator.stopAnimating()
                    self?.handleSuccess(response: response, request: request)
                case .failure(let error):
                     SalesforceLogger.d(RootViewController.self, message:"Error invoking: \(request) , \(error)")
                    self?.indicator.stopAnimating()
                    self?.loadFromStore(name: "Catalog")
            }
        }
        
        // Order request
        
//        let dataString = "{\"order\": [{\"attributes\": {\"type\": \"Order\"},\"EffectiveDate\": \"2020-07-07\"," +
//              "\"Status\": \"Draft\"," +
//              "\"accountId\": \"0011U00001M7hfnQAB\",\"Pricebook2Id\": \"01s1U00000AQqUIQA1\""  +
//                    "}]}"
//
//        if let data = dataString.data(using: .utf8) {
//
//            let request_order = RestRequest(method: .POST, serviceHostType: .instance, path: "/services/data/v30.0/commerce/sale/order", queryParams: nil)
//
//            request_order.setCustomRequestBodyData(data, contentType: "application/json")
//
//            RestClient.shared.send(request: request_order) { [weak self] (result) in
//                switch result {
//                    case .success(let response):
//                        //self?.handleSuccess(response: response, request: request_order)
//                        SalesforceLogger.d(RootViewController.self, message:"Success invoking: \(request_order) , \(response)")
//                    case .failure(let error):
//                        SalesforceLogger.d(RootViewController.self, message:"Error invoking: \(request_order) , \(error)")
//                }
//            }
//        }

        
        

  

    }
    
    override func viewDidLoad() {
        self.tableView.register(CustomCell.self, forCellReuseIdentifier: "CellIdentifier")
        
        self.rowHeight = UIScreen.main.traitCollection.userInterfaceIdiom == .phone ? 152 : 75
        self.tableView.rowHeight = self.rowHeight

        floatingButton()

        indicator.center = view.center
        self.view.addSubview(indicator)
        self.view.bringSubviewToFront(indicator)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        indicator.startAnimating()


    }
    
    func handleSuccess(response: RestResponse, request: RestRequest) {
//        guard let jsonResponse  = try? response.asJson() as? [String:Any], let records = jsonResponse["records"] as? [[String:Any]]  else {
//                SalesforceLogger.d(RootViewController.self, message:"Empty Response for : \(request)")
//                return
//        }
        
        guard let jsonResponse  = try? response.asJson() as? [[String:Any]], let records = jsonResponse as? [[String:Any]]  else {
                       SalesforceLogger.d(RootViewController.self, message:"Empty Response for : \(request)")
                       return
               }

        filteredRecords = [[String:Any]]()

        for record in records{


            let attr = record["attributes"] as? NSDictionary

            if attr!["type"] == nil || record["Name"] == nil {
                continue
            }

            let name = record["Name"] as! String



            let flag = name.contains("DB ") as! Bool








            if attr!["type"] as! String == "Account" {
                continue
            }

            if flag {
                filteredRecords.append(record)

            }




        }




        if ((self.store!.soupExists(forName: "Catalog"))) {
            self.store!.clearSoup("Catalog")
            self.store!.upsert(entries: filteredRecords, forSoupNamed: "Catalog")
            self.loadFromStore(name: "Catalog")
            os_log("\nSmartStore loaded records.", log: self.mylog, type: .debug)
        }
        else{
            createAccountsSoup(soupName: "Catalog")
            self.store!.clearSoup("Catalog")
            self.store!.upsert(entries: filteredRecords, forSoupNamed: "Catalog")
            self.loadFromStore(name: "Catalog")
            os_log("\nSmartStore loaded records.", log: self.mylog, type: .debug)
        }



        SalesforceLogger.d(type(of:self), message:"Invoked: \(request)")
        DispatchQueue.main.async {
            let records_dict = self.filteredRecords as [NSDictionary]
           self.dataRows = records_dict
           self.tableView.reloadData()
       }


    }

    
    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return self.dataRows.count
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
       let footerView = UIView()
        footerView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
       footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height:
       100)
       let button = UIButton()
       button.frame = CGRect(x: 20, y: 10, width: 300, height: 50)
       button.setTitle("Create Order", for: .normal)
        button.addTarget(self, action: #selector(RootViewController.loginAction), for: .touchUpInside)
        button.setTitleColor( #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
       //footerView.addSubview(button)
       return footerView
    }
    
    @objc func loginAction()
    {
        
        SalesforceLogger.d(RootViewController.self, message:"Button is clicked")
        
           // Order request
                
                let dataString = "{\"order\": [{\"attributes\": {\"type\": \"Order\"},\"EffectiveDate\": \"2020-07-10\"," +
                      "\"Status\": \"Draft\"," +
                      "\"accountId\": \"0011U00001M7hfnQAB\",\"Pricebook2Id\": \"01s1U00000AQqUIQA1\","  +
                    "\"OrderItems\": {\"records\": [{\"attributes\": {" +
                    "\"type\": \"OrderItem\"},\"PricebookEntryId\": \"    01u1U00000FGBpcQAH\",\"quantity\": \"1\"," +
                    "\"UnitPrice\": \"12.00\"}]}}]}"
        
                if let data = dataString.data(using: .utf8) {
        
                    let request_order = RestRequest(method: .POST, serviceHostType: .instance, path: "/services/data/v30.0/commerce/sale/order", queryParams: nil)
        
                    request_order.setCustomRequestBodyData(data, contentType: "application/json")
        
                    RestClient.shared.send(request: request_order) { [weak self] (result) in
                        switch result {
                            case .success(let response):
                                //self?.handleSuccess(response: response, request: request_order)
                                SalesforceLogger.d(RootViewController.self, message:"Success invoking: \(request_order) , \(response)")
                            case .failure(let error):
                                SalesforceLogger.d(RootViewController.self, message:"Error invoking: \(request_order) , \(error)")
                        }
                    }
                }
        let storyboard = UIStoryboard(name: "LaunchScreen", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderScreen")
                   self.present(vc, animated: true, completion: nil);
        
//        let storyboard = UIStoryboard(name: "OrdersStoryboard", bundle: nil);
//        let vc = storyboard.instantiateViewController(withIdentifier: "OrderStory")
//                   self.present(vc, animated: true, completion: nil);
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellIdentifier"
        
        // Dequeue or create a cell of the appropriate type.
//        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier) ?? UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        
        self.cell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier) as! CustomCell
        

        
        // Configure the cell to show the data.
        let obj = dataRows[indexPath.row]
       // let attr = obj["attributes"] as! NSDictionary
//        for key in obj.allKeys {
//           SalesforceLogger.d(RootViewController.self, message:key as! String)
//
//
//        }
//        for key in attr.allKeys {
//           SalesforceLogger.d(RootViewController.self, message:key as! String)
//
//
//        }
        
        
                // If you want to add an image to your cell, here's how.
        //        let image = UIImage(named: "icon.png")
        //        cell.imageView?.image = image
        
        //cell.textLabel?.text = obj["Name"] as? String

        //let pbEntry = obj["PricebookEntries"] as? NSDictionary

        self.cell.name = obj["Name"] as? String
        self.cell.desc = obj["Description"] as? String
        let imageData = obj["DisplayUrl"] as? String

        if imageData != nil {
            let array = imageData!.components(separatedBy: ",")
//            let url = URL(string: url_val! )!
//            //sync processing
            var image: UIImage = imageUtils.decodeImageFromBase64(strBase64: array[1])
            self.cell.img = image
//            let data = try? Data(contentsOf: url)
//            self.cell.img = UIImage(data: data!)
//            //Async processing
            //downloadImage(from: url)

            //decodeImageFromBase64(data: array[1])


        }
//        let card = Card(name: obj["Name"] as? String, description: obj["Description"] as? String )
//        SalesforceLogger.d(RootViewController.self, message:card.name!)
//        SalesforceLogger.d(RootViewController.self, message:card.description!)
//        self.cell.dataCard = card
        
        // This adds the arrow to the right hand side.
        //cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        return self.cell
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { [weak self] in
                self?.cell?.img = UIImage(data: data)
            }
        }
    }

    func decodeImageFromBase64(data: String){

        var image: UIImage = imageUtils.decodeImageFromBase64(strBase64: data)
        DispatchQueue.main.async() { [weak self] in
            self?.cell?.img = image
        }

    }


    func loadFromStore(name: String) {
//        querySpec = QuerySpec.buildSmartQuerySpec(
//                smartSql: "select * from {Account}", pageSize: 10)

        let query = QuerySpec.buildAllQuerySpec(soupName: name, orderPath: "Name", order: QuerySpec.sortOrder(from: "ascending"), pageSize: 10)
//        if let querySpec = QuerySpec.buildSmartQuerySpec(
//                smartSql: "select * from {Account}", pageSize: 10),
        if let smartStore = self.store,
           let records = try? smartStore.query(using: query,
                   startingFromPageIndex: 0) as? [[String:Any]] {

            os_log("Placeholder", log: self.mylog, type: .debug)

            DispatchQueue.main.async {
                let records_dict = records as [NSDictionary]
                self.dataRows = records_dict
                self.tableView.reloadData()
            }
        }


    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  off = scrollView.contentOffset.y
        btnFloat.frame = CGRect(x: 285, y:   off + 485, width: btnFloat.frame.size.width, height: btnFloat.frame.size.height)
    }

    func floatingButton(){
        btnFloat = UIButton(type: .custom)
        btnFloat.frame = CGRect(x: 135, y: 485, width: 80, height: 80)
        btnFloat.setTitle("Add", for: .normal)
        btnFloat.backgroundColor = UIColor.orange
        btnFloat.clipsToBounds = true
        btnFloat.layer.cornerRadius = 50
        btnFloat.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnFloat.layer.borderWidth = 3.0
        btnFloat.addTarget(self,action: #selector(RootViewController.showToast), for: .touchUpInside)
        view.addSubview(btnFloat)
    }

    func createAccountsSoup(soupName: String) {

        guard let  index1 = SoupIndex(path: "Name", indexType: "String",
                columnName: "Name"),
              let  index2 = SoupIndex(path: "Id", indexType: "String",
                      columnName: "Id")
                else {
            return
        }

        do {
            try store!.registerSoup(withName: soupName, withIndices:[index1,index2])
        } catch (let error) {
            SalesforceLogger.d(RootViewController.self, message:"Couldn’t create soup \(soupName).Error: \(error.localizedDescription)")
            return
        }
    }


    @objc func showToast(){
        let alert = UIAlertController(title: nil, message: "Item added to cart", preferredStyle: .alert)
        alert.view.backgroundColor = .black
        alert.view.alpha = 0.5
        alert.view.layer.cornerRadius = 15
        self.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            alert.dismiss(animated: true)
        }
    }


}
