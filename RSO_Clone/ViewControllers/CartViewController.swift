//
// Created by Sagar Das on 8/11/20.
// Copyright (c) 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation
import UIKit
class CartViewController : UITableViewController{

    var cell: CartUIView!
    var btnFloat: UIButton!

    override func loadView() {
        super.loadView()


    }

    override func viewDidLoad() {
        super.viewDidLoad()
        floatingButton()
    }

    override func tableView(_ tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return Cart.productList.count
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellIdentifier"

        // Dequeue or create a cell of the appropriate type.
//        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier) ?? UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)

        let product: Product = Cart.productList[indexPath.row]
        self.cell = CartUIView()


        self.cell.name = product.name
        self.cell.desc = product.description

        return self.cell
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  off = scrollView.contentOffset.y
        btnFloat.frame = CGRect(x: 285, y:   off + 485, width: btnFloat.frame.size.width, height: btnFloat.frame.size.height)
    }

    func floatingButton(){
        btnFloat = UIButton(type: .custom)
        btnFloat.frame = CGRect(x: 135, y: 485, width: 100, height: 100)
        btnFloat.setTitle("New", for: .normal)
        btnFloat.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        btnFloat.clipsToBounds = true
        btnFloat.layer.cornerRadius = 50
        btnFloat.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnFloat.layer.borderWidth = 3.0
        //btn.addTarget(self,action: #selector(DestinationVC.buttonTapped), for: UIControlEvent.touchUpInside)
        view.addSubview(btnFloat)
    }


}
