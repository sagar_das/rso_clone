//
// Created by Sagar Das on 8/10/20.
// Copyright (c) 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation
import UIKit
import SalesforceSDKCore
import SmartStore
var utils = UtilOperations()
var indicator: UIActivityIndicatorView!

class AccountViewController : UITableViewController{


    var dataRows = [NSDictionary]()
    var store = SmartStore.shared(withName: SmartStore.defaultStoreName)
    let mylog = OSLog(subsystem: "com.commercecx.rso", category: "tutorial")
    var cell: AccountsUIView!
    var rowHeight : CGFloat!
    var imageUtils = ImageUtils()
    var networkUtils = NetworkUtils()
    var requestUtils = RequestUtils()
    var utils = UtilOperations()
    var indicator: UIActivityIndicatorView!
    var filteredRecords: [[String:Any]]!




    // MARK: - View lifecycle
    override func loadView() {
        super.loadView()

        indicator = utils.fetchLoadingBar()

        let request = requestUtils.createRestRequest(path: networkUtils.productPath, type: "GET")



        RestClient.shared.send(request: request) { [weak self] (result) in
            switch result {
            case .success(let response):
                self?.indicator.stopAnimating()
                self?.handleSuccess(response: response, request: request)
            case .failure(let error):
                SalesforceLogger.d(RootViewController.self, message:"Error invoking: \(request) , \(error)")
                self?.indicator.stopAnimating()
                self?.loadFromStore(name: "Account")

            }
        }








    }

    override func viewDidLoad() {
        self.tableView.register(AccountsUIView.self, forCellReuseIdentifier: "CellIdentifier")

        self.rowHeight = UIScreen.main.traitCollection.userInterfaceIdiom == .phone ? 152 : 75
        self.tableView.rowHeight = self.rowHeight

        indicator.center = view.center
        self.view.addSubview(indicator)
        self.view.bringSubviewToFront(indicator)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        indicator.startAnimating()
    }

    func handleSuccess(response: RestResponse, request: RestRequest) {


        guard let jsonResponse  = try? response.asJson() as? [[String:Any]], let records = jsonResponse as? [[String:Any]]  else {
            SalesforceLogger.d(RootViewController.self, message:"Empty Response for : \(request)")
            return
        }

        filteredRecords = [[String:Any]]()

        for record in records{


                let attr = record["attributes"] as? NSDictionary

                if attr!["type"] == nil {
                    continue
                }



                if attr!["type"] as! String == "Account" {
                    filteredRecords.append(record)
                }




        }



        self.store!.allSoupNames()
        if ((self.store!.soupExists(forName: "Account"))) {
            self.store!.clearSoup("Account")
            self.store!.upsert(entries: filteredRecords, forSoupNamed: "Account")
            self.loadFromStore(name: "Account")
            os_log("\nSmartStore loaded records.", log: self.mylog, type: .debug)
        }
        else{
            createAccountsSoup(soupName: "Account")
            self.store!.clearSoup("Account")
            self.store!.upsert(entries: filteredRecords, forSoupNamed: "Account")
            self.loadFromStore(name: "Account")
            os_log("\nSmartStore loaded records.", log: self.mylog, type: .debug)
        }



        SalesforceLogger.d(type(of:self), message:"Invoked: \(request)")
        DispatchQueue.main.async {
            let records_dict = self.filteredRecords as [NSDictionary]
            self.dataRows = records_dict
            self.tableView.reloadData()
        }
    }


    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return self.dataRows.count
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height:
        100)
        let button = UIButton()
        button.frame = CGRect(x: 20, y: 10, width: 300, height: 50)
        button.setTitle("Create Order", for: .normal)
        button.addTarget(self, action: #selector(RootViewController.loginAction), for: .touchUpInside)
        button.setTitleColor( #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        //footerView.addSubview(button)
        return footerView
    }

    @objc func loginAction()
    {

        SalesforceLogger.d(RootViewController.self, message:"Button is clicked")

        // Order request

        let dataString = "{\"order\": [{\"attributes\": {\"type\": \"Order\"},\"EffectiveDate\": \"2020-07-10\"," +
                "\"Status\": \"Draft\"," +
                "\"accountId\": \"0011U00001M7hfnQAB\",\"Pricebook2Id\": \"01s1U00000AQqUIQA1\","  +
                "\"OrderItems\": {\"records\": [{\"attributes\": {" +
                "\"type\": \"OrderItem\"},\"PricebookEntryId\": \"    01u1U00000FGBpcQAH\",\"quantity\": \"1\"," +
                "\"UnitPrice\": \"12.00\"}]}}]}"

        if let data = dataString.data(using: .utf8) {

            let request_order = RestRequest(method: .POST, serviceHostType: .instance, path: "/services/data/v30.0/commerce/sale/order", queryParams: nil)

            request_order.setCustomRequestBodyData(data, contentType: "application/json")

            RestClient.shared.send(request: request_order) { [weak self] (result) in
                switch result {
                case .success(let response):
                    //self?.handleSuccess(response: response, request: request_order)
                    SalesforceLogger.d(RootViewController.self, message:"Success invoking: \(request_order) , \(response)")
                case .failure(let error):
                    SalesforceLogger.d(RootViewController.self, message:"Error invoking: \(request_order) , \(error)")
                }
            }
        }
        let storyboard = UIStoryboard(name: "LaunchScreen", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "OrderScreen")
        self.present(vc, animated: true, completion: nil);

//        let storyboard = UIStoryboard(name: "OrdersStoryboard", bundle: nil);
//        let vc = storyboard.instantiateViewController(withIdentifier: "OrderStory")
//                   self.present(vc, animated: true, completion: nil);

    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellIdentifier"

        // Dequeue or create a cell of the appropriate type.
//        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier) ?? UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)

        self.cell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier) as! AccountsUIView




        let obj = dataRows[indexPath.row]

        self.cell.name = obj["Name"] as? String
        self.cell.desc = obj["Description"] as? String

        return self.cell

    }

    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }





    func loadFromStore(name: String) {

        let query = QuerySpec.buildAllQuerySpec(soupName: name, orderPath: "Name", order: QuerySpec.sortOrder(from: "ascending"), pageSize: 10)

        if let smartStore = self.store,
           let records = try? smartStore.query(using: query,
                   startingFromPageIndex: 0) as? [[String:Any]] {

            os_log("Placeholder", log: self.mylog, type: .debug)

            DispatchQueue.main.async {
                let records_dict = records as [NSDictionary]
                self.dataRows = records_dict
                self.tableView.reloadData()
            }
        }


    }

    func createAccountsSoup(soupName: String) {

              guard let  index1 = SoupIndex(path: "Name", indexType: "String",
                      columnName: "Name"),
              let  index2 = SoupIndex(path: "Id", indexType: "String",
                      columnName: "Id")
                else {
            return
        }

        do {
            try store!.registerSoup(withName: soupName, withIndices:[index1,index2])
        } catch (let error) {
            SalesforceLogger.d(RootViewController.self, message:"Couldn’t create soup \(soupName).Error: \(error.localizedDescription)")
            return
        }
    }




}