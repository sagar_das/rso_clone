//
//  ViewRouter.swift
//  RSO_Clone
//
//  Created by Sagar Das on 7/23/20.
//  Copyright © 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation
import Foundation
import Combine
import SwiftUI

class ViewRouter: ObservableObject {
    
    let objectWillChange = PassthroughSubject<ViewRouter,Never>()
    
    var currentPage: String = "PrevOrders" {
        didSet {
            objectWillChange.send(self)
        }
    }
}
