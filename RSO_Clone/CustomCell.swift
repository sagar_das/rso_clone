//
//  CustomCell.swift
//  RSO_Clone
//
//  Created by Sagar Das on 6/29/20.
//  Copyright © 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class CustomCell: UITableViewCell{
    
    var name: String?
    var desc: String?
    var img: UIImage?
    //var dataCard: Card?



    
    var nameView : UILabel = {
        var textView = UILabel()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.font = .systemFont(ofSize: 16)
        return textView
    }()
    
    var descView : UILabel = {
        var descTextView = UILabel()
        descTextView.translatesAutoresizingMaskIntoConstraints = false
        return descTextView
    }()
    
    var imgView : UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.frame = CGRect(x: 135, y: 485, width: 50, height: 50)
        return imageView
    }()
    
    
    
    
    var body : some View {
        ZStack{
        RoundedRectangle(cornerRadius: 25, style: .continuous)
            .fill(Color.white)
            .shadow(radius: 10)
            .frame(width: 450, height: 250)
            }
    }
    
    
    
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
       
        
        let cb5 = CheckBox.init()
        cb5.frame = CGRect(x: 25, y: 25, width: 20, height: 20)
        cb5.style = .tick
        cb5.borderStyle = .roundedSquare(radius: 5)
        cb5.addTarget(self, action: #selector(onCheckBoxValueChange(_:)), for: .valueChanged)

        self.addSubview(imgView)
        self.addSubview(nameView)
        self.addSubview(descView)
        self.addSubview(cb5)
        
        
        cb5.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        cb5.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        cb5.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        cb5.topAnchor.constraint(equalTo: self.topAnchor).isActive = true


        imgView.leftAnchor.constraint(equalTo: cb5.rightAnchor).isActive = true
        imgView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        imgView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        imgView.widthAnchor.constraint(equalTo: self.heightAnchor).isActive = true

        nameView.leftAnchor.constraint(equalTo: self.imgView.rightAnchor).isActive = true
        nameView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        nameView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        nameView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true

        descView.leftAnchor.constraint(equalTo: self.imgView.rightAnchor).isActive = true
        descView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        descView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        descView.topAnchor.constraint(equalTo: self.nameView.bottomAnchor).isActive = true
        
        
        




    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if let name = name {
            nameView.text = name
        }
        if let desc = desc {
            descView.text = desc
        }
        if let img = img {
            imgView.image = img
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
    
        fatalError("init(coder) has not be implemented")
    }
    
    @objc func onCheckBoxValueChange(_ sender: CheckBox) {
        
        //print(sender.isChecked)
        if(sender.isChecked){
            let product = Product(name: self.name!, description: self.desc!)
            Cart.productList.append(product)
        }

    }
    
    
    
}

