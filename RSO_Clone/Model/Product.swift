//
// Created by Sagar Das on 8/13/20.
// Copyright (c) 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation
class Product{
    var name: String!
    var description: String!


    init(name: String, description: String){
        self.name = name
        self.description = description
    }
}