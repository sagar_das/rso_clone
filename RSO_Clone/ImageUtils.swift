//
// Created by Sagar Das on 8/10/20.
// Copyright (c) 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation
import UIKit
public class ImageUtils{

    func decodeImageFromBase64(strBase64: String) -> UIImage{

        let imageData = Data.init(base64Encoded: strBase64, options: .init(rawValue: 0))
        let image = UIImage(data: imageData!)
        return image!

}
}