//
// Created by Sagar Das on 8/10/20.
// Copyright (c) 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation
import SalesforceSDKCore
public class RequestUtils{

    func createRestClientRequest(query: String) -> RestRequest {

        //var request = RestClient.shared.request(forQuery: "SELECT Name, Description,DisplayUrl FROM Product2 LIMIT 3", apiVersion: SFRestDefaultAPIVersion)
        return RestClient.shared.request(forQuery: query, apiVersion: SFRestDefaultAPIVersion)
    }

    func createRestRequest(path: String, type: String) -> RestRequest{

        if type == "GET" {
            return RestRequest(method: .GET, serviceHostType: .custom,
                    path: path,
                    queryParams: nil)
        }
        else {
            return RestRequest(method: .POST, serviceHostType: .custom,
                    path: path,
                    queryParams: nil)
        }
    }

    func createCustomURLRestRequest(url: String) -> RestRequest{

        //        let request =
//            RestRequest.customUrlRequest(with: RestRequest.Method.GET,
//                                      baseURL: baseURL,
//                                         path: productPath,
//                                  queryParams: nil)


              return   RestRequest.customUrlRequest(with: RestRequest.Method.GET,
                                      baseURL: url,
                                         path: url,
                                  queryParams: nil)
    }

    func createBatchRestRequest(requests: [RestRequest]){

        let builder = BatchRequestBuilder()
        builder.setHaltOnError(true)

        for request in requests{
            builder.add(request as! BatchRequest)
        }




    }



}