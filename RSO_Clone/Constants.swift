//
// Created by Sagar Das on 8/10/20.
// Copyright (c) 2020 RSO_CloneOrganizationName. All rights reserved.
//

import UIKit

struct Constants {

    struct TabBarImageName {
        static let tabBar0 = "gamecontroller.fill"
        static let tabBar1 = "person.fill"
        static let tabBar2 = "text.justify"
        static let tabBar3 = "cart.fill"
    }

    struct TabBarText {
        static let tabBar0 = "Orders"
        static let tabBar1 = "Accounts"
        static let tabBar2 = "Catalog"
        static let tabBar3 = "Cart"
    }
}

