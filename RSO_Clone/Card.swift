//
//  Card.swift
//  RSO_Clone
//
//  Created by Sagar Das on 6/29/20.
//  Copyright © 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation

struct Card{
    
    let name: String?
    var description: String?
    
    static var example: Card {
        Card(name:"Demo name", description:"Demo desc")
    }
}
