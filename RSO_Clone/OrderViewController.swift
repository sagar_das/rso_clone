//
//  OrderViewController.swift
//  RSO_Clone
//
//  Created by Sagar Das on 7/8/20.
//  Copyright © 2020 RSO_CloneOrganizationName. All rights reserved.
//

import Foundation
import UIKit
import SalesforceSDKCore
import SmartStore

class OrderViewController: UITableViewController{

    var dataRows = [NSDictionary]()
    var store = SmartStore.shared(withName: SmartStore.defaultStoreName)
    let mylog = OSLog(subsystem: "com.commercecx.rso", category: "tutorial")
    var cell: OrdersUIView!
    var btnFloat: UIButton!
    var networkUtils = NetworkUtils()
    var requestUtils = RequestUtils()
    var utils = UtilOperations()
    var indicator: UIActivityIndicatorView!
    var rowHeight : CGFloat!

    override func loadView() {
        super.loadView()

        indicator = utils.fetchLoadingBar()


        //let request = requestUtils.createRestRequest(path: networkUtils.orderPath, type: "GET")

        let request = RestClient.shared.request(forQuery: "SELECT OrderNumber FROM Order LIMIT 10", apiVersion: SFRestDefaultAPIVersion)



        RestClient.shared.send(request: request) { [weak self] (result) in
            switch result {
            case .success(let response):
                self?.indicator.stopAnimating()
                self?.handleSuccess(response: response, request: request)
            case .failure(let error):
                SalesforceLogger.d(RootViewController.self, message:"Error invoking: \(request) , \(error)")
                self?.indicator.stopAnimating()
                //self?.loadFromStore(name: "Orders")

            }
        }




    }

    override func viewDidLoad() {
        self.tableView.register(OrdersUIView.self, forCellReuseIdentifier: "CellIdentifier")

        self.rowHeight = UIScreen.main.traitCollection.userInterfaceIdiom == .phone ? 152 : 75
        self.tableView.rowHeight = self.rowHeight

        floatingButton()

        indicator.center = view.center
        self.view.addSubview(indicator)
        self.view.bringSubviewToFront(indicator)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        indicator.startAnimating()
    }

    func handleSuccess(response: RestResponse, request: RestRequest) {


//        guard let jsonResponse  = try? response.asJson() as? [[String:Any]], let records = jsonResponse as? [[String:Any]]  else {
//            SalesforceLogger.d(RootViewController.self, message:"Empty Response for : \(request)")
//            return
//        }

                guard let jsonResponse  = try? response.asJson() as? [String:Any], let records = jsonResponse["records"] as? [[String:Any]]  else {
                SalesforceLogger.d(RootViewController.self, message:"Empty Response for : \(request)")
                return
        }



        self.store!.allSoupNames()
        if ((self.store!.soupExists(forName: "Orders"))) {
            self.store!.clearSoup("Orders")
            self.store!.upsert(entries: records, forSoupNamed: "Orders")
            self.loadFromStore(name: "Orders")
            os_log("\nSmartStore loaded records.", log: self.mylog, type: .debug)
        }
        else{
            createAccountsSoup(soupName: "Orders")
            self.store!.clearSoup("Orders")
            self.store!.upsert(entries: records, forSoupNamed: "Orders")
            self.loadFromStore(name: "Orders")
            os_log("\nSmartStore loaded records.", log: self.mylog, type: .debug)
        }



        SalesforceLogger.d(type(of:self), message:"Invoked: \(request)")
        DispatchQueue.main.async {
            let records_dict = records as [NSDictionary]
            self.dataRows = records_dict
            self.tableView.reloadData()
        }
    }

    override func tableView(_ tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        //return 1
        return self.dataRows.count
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellIdentifier"

        // Dequeue or create a cell of the appropriate type.
//        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier) ?? UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)

        //self.cell = OrdersUIView()

        self.cell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier) as! OrdersUIView

        let obj = dataRows[indexPath.row]

//        self.cell.name = "Order 1"
//        self.cell.desc = "Description"

        self.cell.name = obj["OrderNumber"] as? String


        return self.cell
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let  off = scrollView.contentOffset.y
        btnFloat.frame = CGRect(x: 285, y:   off + 485, width: btnFloat.frame.size.width, height: btnFloat.frame.size.height)
    }

    func loadFromStore(name: String) {


        let query = QuerySpec.buildAllQuerySpec(soupName: name, orderPath: "Name", order: QuerySpec.sortOrder(from: "ascending"), pageSize: 10)

        if let smartStore = self.store,
           let records = try? smartStore.query(using: query,
                   startingFromPageIndex: 0) as? [[String:Any]] {

            os_log("Placeholder", log: self.mylog, type: .debug)

            DispatchQueue.main.async {
                let records_dict = records as [NSDictionary]
                self.dataRows = records_dict
                self.tableView.reloadData()
            }
        }


    }

    func createAccountsSoup(soupName: String) {

        guard let  index1 = SoupIndex(path: "Name", indexType: "String",
                columnName: "Name"),
              let  index2 = SoupIndex(path: "Id", indexType: "String",
                      columnName: "Id")
                else {
            return
        }

        do {
            try store!.registerSoup(withName: soupName, withIndices:[index1,index2])
        } catch (let error) {
            SalesforceLogger.d(RootViewController.self, message:"Couldn’t create soup \(soupName).Error: \(error.localizedDescription)")
            return
        }
    }

    func floatingButton(){
        btnFloat = UIButton(type: .custom)
        btnFloat.frame = CGRect(x: 135, y: 485, width: 100, height: 100)
        btnFloat.setTitle("New", for: .normal)
        btnFloat.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        btnFloat.clipsToBounds = true
        btnFloat.layer.cornerRadius = 50
        btnFloat.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        btnFloat.layer.borderWidth = 3.0
        btnFloat.addTarget(self,action: #selector(OrderViewController.createOrder), for: .touchUpInside)
        view.addSubview(btnFloat)
    }

    @objc func createOrder(){

        let sourceOrderId = utils.fetchUnixEpoch()

        let request = requestUtils.createRestRequest(path: networkUtils.orderPath, type: "POST")

        let dataString = "{\"body\" : {\"messageSegments\" : [{\"type\" : \"Text\", " +
                "\"text\" : \"Some Comment\"}]}, \"feedElementType\":\"FeedItem\", " +
                "\"subjectId\":\"me\"}"

        let orderDataString = "{ " +
                "\"AccountInfo\": { " +
                " \"AccountName\" : \"Test Account\", " +
                " \"AccountID\" : \"0011U00001Q8lXJQAZ\" " +
                " }, " +
                "\"Orders\" : [{ " +
                "\"SourceOrderNumber\": " + "\"" + sourceOrderId + "\", " +
                "\"PriceBook2Id\": \"01s1U00000AQqxiQAD\", " +
                "\"OrderDescription\": \"test order desc\", " +
                "\"ContractId\": \"8001U000000IUGKQA4\", " +
                "\"BillingStreet\" : \"Wilhelminaplein\", " +
                "\"BillingState\" : \"Friesland\", " +
                "\"BillingCity\" : \"Friesland\", " +
                "\"BillingPostalCode\" : \"8911 BS\", " +
                "\"BillingCountry\" : \"NETHERLANDS\", " +
                "\"ShippingStreet\" : \"Wilhelminaplein\", " +
                "\"ShippingState\" : \"Friesland\", " +
                "\"ShippingCity\" : \"Friesland\", " +
                "\"ShippingPostalCode\" : \"8911 BS\", " +
                "\"ShippingCountry\" : \"NETHERLANDS\", " +
                "\"StartDate\" : \"2020-08-17T11:46:00.000Z\" " +
                " }], " +
                "\"Products\" : [{ " +
                "\"ProductFamily\" : \"Test\", " +
                "\"ProductName\" : \"PICKW GRN TEA OR LEM 100X2GX6\", " +
                "\"ProductImage\" : \"FileDownload?file=null\", " +
                "\"ProductID\" : \"01t1U000006NYftQAG\", " +
                "\"ProductDescription\" : \"Desc\", " +
                "\"PriceBookEntryId\": \"01u1U00000FGFSHQA5\", " +
                "\"UnitPrice\": \"12.00\", " +
                "\"IsActive\" : true, " +
                "\"Quantity\" : \"10\" " +
                "}]" +
                "}"

        let data = orderDataString.data(using: .utf8)
        request.setCustomRequestBodyData(data!, contentType: "application/json")



        RestClient.shared.send(request: request) { [weak self] (result) in
            switch result {
            case .success(let response):
                self?.indicator.stopAnimating()
                self?.handleSuccess(response: response, request: request)
            case .failure(let error):
                SalesforceLogger.d(RootViewController.self, message:"Error invoking: \(request) , \(error)")
                self?.indicator.stopAnimating()
                    //self?.loadFromStore(name: "Orders")

            }
        }


    }


}

